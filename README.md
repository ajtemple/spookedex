# Spookedex

A web app for filtering Phasmophobia ghost types based on evidence. Hosted at [spookedex.com](https://spookedex.com/)

I mostly made this to practice filtering data using React.

## Built with

- [Create React App](https://create-react-app.dev/) - bootstrapping the React project
- [Recoil](https://www.npmjs.com/package/recoil) - state management
- [TailwindCSS](https://tailwindcss.com/) - stylesheets
- [Vercel](https://vercel.com/) - hosting

### Planned features

- On/off/default states for each evidence type, so that options can be ruled out as well as confirmed. This will require a rewrite of a lot of the filtering logic.
- Hide strength/weakness section by default and add a button to expand it. Will do this once I figure out how to do it without changing the height of the entire row.

### Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

### License

[MIT License](https://choosealicense.com/licenses/mit/)
