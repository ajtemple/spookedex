import { RecoilRoot } from 'recoil';

import Header from './components/Header';
import GhostList from './components/GhostList';

const App = () => (
  <RecoilRoot>
    <div className="App bg-gray-300 h-screen">
      <Header />
      <GhostList />
    </div>
  </RecoilRoot>
);

export default App;
