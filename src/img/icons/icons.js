import { ReactComponent as emfIcon } from './emf.svg';
import { ReactComponent as orbIcon } from './orb.svg';
import { ReactComponent as writingIcon } from './writing.svg';
import { ReactComponent as fingerprintIcon } from './fingerprint.svg';
import { ReactComponent as snowflakeIcon } from './snowflake.svg';
import { ReactComponent as radioIcon } from './radio.svg';
import { ReactComponent as dotsIcon } from './dots.svg';

const iconArray = [
	emfIcon,
	orbIcon,
	writingIcon,
	fingerprintIcon,
	snowflakeIcon,
	radioIcon,
	dotsIcon
];

export default iconArray;