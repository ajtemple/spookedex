import { ReactComponent as Logo } from '../img/icons/ghost.svg';

import EvidenceFilter from './EvidenceFilter';

const Header = () => (
	<div className="Header w-full bg-gray-700 py-4">
		<div className="Header-container container mx-auto grid grid-rows-2">
		  <div className="Logo-wrapper container text-center">
				<Logo className="h-20 w-20 fill-current text-gray-300 mx-auto lg:mx-0 sm:float-left" />
				<h1 className="text-gray-300 text-3xl font-thin px-2 sm:float-left sm:pt-4">Spookedex</h1>
			</div>
			
			<EvidenceFilter />
		</div>
	</div>
);

export default Header;