import { useRecoilValue } from 'recoil';

import Ghost from './Ghost';

import selectedEvidenceState from '../state/selectedEvidence';
import ghostTypes from '../data/ghostTypes';

// generate item keys
let itemKey = 0;
const getItemKey = () => itemKey++;

// ghost list component
const GhostList = () => {
	const selectedEvidence = useRecoilValue(selectedEvidenceState);

	const filterGhosts = () => {
		return ghostTypes.filter(ghost => selectedEvidence.every(v => ghost.evidence.includes(v)));
	}

	return (
		<div className="GhostList w-full h-auto bg-gray-300 text-gray-700">
			<div className="GhostList-Wrapper container mx-auto px-3 py-5 flex flex-row flex-wrap">
				{filterGhosts().map(ghostType => {
					return (
						<Ghost key={getItemKey()} name={ghostType.name} evidence={ghostType.evidence} description={ghostType.description} strength={ghostType.strength} weakness={ghostType.weakness} />
					)
				})}
			</div>
		</div>
	)
}
export default GhostList;

// grid gap-4 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3