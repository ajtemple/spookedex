import { useRecoilValue } from 'recoil';
import { useState } from 'react';

import selectedEvidenceState from '../state/selectedEvidence';

import evidence from '../data/evidence';
import iconArray from '../img/icons/icons';

import '../css/Ghost.css';

const Ghost = (props) => {
	const selectedEvidence = useRecoilValue(selectedEvidenceState);

	return (
		<div className="Ghost bg-gray-200 shadow-lg overflow-hidden rounded relative max-w-sm m-3 flex-initial">
			<div className="grid grid-cols-2">

				<div className="px-4 py-2 block">
					<h3 className="font-normal text-xl mb-2">{props.name}</h3>
				</div>

				<div className="px-4 py-2">
					<ul className="text-right">
						{props.evidence.map(evIndex => {
							// grab evidence name and icon
							const ev = evidence[evIndex];
							const Icon = iconArray[evIndex];
							const isSelected = selectedEvidence.indexOf(evIndex) !== -1;

							return <li className={`text-xs ${isSelected? 'text-gray-400' : ''}`}>{ev}&nbsp; <Icon className="h-5 w-5 fill-current text-gray-500 inline" /></li>
						})}
					</ul>
				</div>

			</div>

			<div className="px-4 py-2 z-40 bg-gray-200">
				<p className="text-xs text-gray-500 tracking-tight leading-tight">{props.description}</p>
			</div>

			<div className={`px-4 py-2 transition-all duration-500 ease-in-out z-0`}>
				<p className="text-xs tracking-tight leading-tight text-gray-600">
					<span className="font-bold">Strength: </span> {props.strength}
					<br />
					<span className="font-bold">Weakness: </span> {props.weakness}
				</p>
			</div>
		</div>
	);
}

export default Ghost;