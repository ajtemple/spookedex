import { useRecoilState } from 'recoil';

import evidence from '../data/evidence';
import ghostTypes from '../data/ghostTypes';
import selectedEvidenceState from '../state/selectedEvidence';

// item key generator
let itemKey = 0;
const getItemKey = () => itemKey++;

// evidence filter component
// contains buttons to add or remove evidence types from state.
const EvidenceFilter = () => {
	const [selectedEvidence, setSelectedEvidence] = useRecoilState(selectedEvidenceState);

	// update evidence when button is clicked.
	// if evidence type already exists in state, remove it. Otherwise, add it.
	const updateEvidence = ({target: {value}}) => {

		// button value sets the evidence type to a string, set it back to base10 int
		const evidenceType = parseInt(value, 10);
		const index = selectedEvidence.indexOf(evidenceType);

		// if selected evidence already exists in state:
		if (index !== -1) {
			// remove selected evidence with slice()
			setSelectedEvidence([
				...selectedEvidence.slice(0, index),
				...selectedEvidence.slice(index + 1)
			]);
		}
		// if selected evidence is not in state:
		else {
			// check if 3 or more pieces of evidence are selected
			if (selectedEvidence.length >= 3) return;
			// add new evidence to state, preserving old state
			setSelectedEvidence(oldEvidence => [
				...oldEvidence,
				evidenceType
			]);
		}
	}

	// check whether a piece of evidence is valid based on currently selected evidence.
	// determines whether or not the button should be enabled for the evidence type specified by index.
	const checkEvidence = (index) => {
		// all buttons disabled if 3 or more pieces of evidence selected
		if (selectedEvidence.length >= 3) return false;
		// create array to check against, containing current state and button index
		const check = [
			...selectedEvidence,
			index
		];

		// check if there is a ghost type which contains the current state, plus the index of the button
		return ghostTypes.some(ghost => check.every(v => ghost.evidence.includes(v)));

	}

	const clearFilter = () => {
		setSelectedEvidence([]);
	}

	return (
		<div className="Filter text-gray-500 flex-row mt-8 px-2 mx-auto container text-center sm:text-left">
			{evidence.map((item, index) => {

				// check if evidence exists in state
				const isSelected = selectedEvidence.indexOf(index) !== -1;
				// check if button should be disabled
				const isDisabled = (!checkEvidence(index) && !isSelected);

				// set button classes based on selected evidence & disabled state
				const btnClasses = (isSelected) 
					? "border-green-300 text-green-300" // SELECTED
					: (isDisabled)
						? "text-gray-800 border-gray-800 cursor-not-allowed" // UNSELECTED, DISABLED
						: "border-gray-500 hover:border-gray-300 hover:text-gray-300" // UNSELECTED, NOT DISABLED

				return <button value={index} key={getItemKey()} onClick={updateEvidence} disabled={isDisabled} className={`px-2 py-1 mr-2 my-1 border rounded focus:outline-none ${btnClasses}`}>{item}</button>
			})}
			{selectedEvidence.length > 0 ? <button onClick={clearFilter} className="px-2 py-1 my-1 border rounded focus:outline-none text-gray-300 bg-red-700 border-red-800">Clear</button> : null}
		</div>
	);
}

export default EvidenceFilter;