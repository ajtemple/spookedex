const ghostTypes = [
	{
		"name": "Spirit",
		"evidence": [0,2,5],
		"description": "A Spirit is the most common Ghost you will come across however it is still very powerful and dangerous. They are usually discovered at one of their hunting grounds after an unexplained death.",
		"strength": "None.",
		"weakness": "Using Smudge Sticks on a Spirit will stop it attacking for 120 seconds instead of 90."
	},
	{
		"name": "Wraith",
		"evidence": [0,5,6],
		"description": "A Wraith is one of the most dangerous Ghosts you will find. It is also the only known ghost that has the ability of flight and has sometimes been known to travel through walls.",
		"strength": "Wraiths can travel through walls and doors, and will not leave footprints on the ground.",
		"weakness": "If a Wraith comes into contact with a pile of salt, it will immediately cease attacking."
	},
	{
		"name": "Phantom",
		"evidence": [3,5,6],
		"description": "A Phantom is a Ghost that can possess the living, most commonly summoned by a Ouija Board. It also induces fear into those around it.",
		"strength": "Looking at a Phantom will considerably drop your Sanity.",
		"weakness": "Taking a photo of the Phantom will make it temporarily disappear."
	},
	{
		"name": "Poltergeist",
		"evidence": [2,3,5],
		"description": "One of the most famous Ghosts, a Poltergeist, also known as a noisy ghost can manipulate objects around it to spread fear into it's victims.",
		"strength": "A Poltergeist is capable of influencing more objects at once than any other Ghosts, and is capable of shutting multiple doors at once.",
		"weakness": "A Poltergeist is almost ineffective in an empty room."
	},
	{
		"name": "Banshee",
		"evidence": [1,3,6],
		"description": "A Banshee is a natural hunter and will attack anything. It has been known to stalk it's prey one at a time until making it's kill.",
		"strength": "A Banshee will focus on one player at a time until it kills them.",
		"weakness": "Banshees fear the Crucifix, which boosts the Hunt-stopping range of one from 3 meters to 5 meters against it."
	},
	{
		"name": "Jinn",
		"evidence": [0,3,4],
		"description": "A Jinn is a territorial Ghost that will attack when threatened. It has also been known to be able to travel at significant speed.",
		"strength": "A Jinn will travel at a faster speed if its victim is far away.",
		"weakness": "Turning off the location's power source will prevent the Jinn from using its ability."
	},
	{
		"name": "Mare",
		"evidence": [1,2,5],
		"description": "A Mare is the source of all nightmares, making it most powerful in the dark.",
		"strength": "Will have an increased chance to attack in the dark.",
		"weakness": "Turning the lights on around the Ghost will lower it's chance to attack."
	},
	{
		"name": "Revenant",
		"evidence": [1,2,4],
		"description": "A Revenant is slow but violent Ghost that will attack indiscriminately. It has been rumored to travel at significantly higher speed when hunting.",
		"strength": "Will travel at a significantly faster speed when hunting a victim.",
		"weakness": "Hiding from the Ghost will cause it to move very slowly."
	},
	{
		"name": "Shade",
		"evidence": [0,2,4],
		"description": "A Shade is known to be a Shy Ghost. There is evidence that a Shade will stop all paranormal activity if there are multiple people nearby.",
		"strength": "Being shy means the Ghost will be harder to find.",
		"weakness": "The Ghost will not enter hunting mode if there is multiple people nearby."
	},
	{
		"name": "Demon",
		"evidence": [2,3,4],
		"description": "A Demon is one of the worst Ghosts you can encounter. It has been known to attack without reason.",
		"strength": "It will attack more often then any other Ghost.",
		"weakness": "Asking it successful questions on the Ouija Board won't lower the users sanity."
	},
	{
		"name": "Yurei",
		"evidence": [1,4,6],
		"description": "A Yurei is a Ghost that has returned to the physical world, usually for the purpose of revenge or hatred.",
		"strength": "Has been known to have a stronger effect on people's sanity.",
		"weakness": "Smudging the Ghost's room will cause it to not wander around the location for a long time."
	},
	{
		"name": "Oni",
		"evidence": [0,4,6],
		"description": "Oni's are cousin to the Demon and posses extreme strength. There have been rumours that they become more active around their prey.",
		"strength": "Are more active when people are nearby and have been seen moving at great speed.",
		"weakness": "Being more active will make the Ghost easier to find and identify."
	},
	{
		"name": "Hantu",
		"evidence": [1,3,4],
		"description": "A rare ghost that can be found in hot climates. They are known to attack more often in cold weather",
		"strength": "Hantu moves faster in colder areas.",
		"weakness": "Hantu moves slower in warmer areas."
	},
	{
		"name": "Yokai",
		"evidence": [1,5,6],
		"description": "A common type of ghost that is attracted to human voices. They can usually be found haunting family homes.",
		"strength": "Talking near a Yokai will anger it and cause it to attack more often.",
		"weakness": "While hunting, it can only hear voices close to it."
	},
	{
		"name": "Goryo",
		"evidence": [0,3,6],
		"description": "Using a video camera is the only way to view a Goryo, when it passes through a D.O.T.S Projector.",
		"strength": "A Goryo will usually only show itself on camera if there are no people nearby",
		"weakness": "They are rarely seen far from their place of death"
	},
	{
		"name": "Myling",
		"evidence": [0,3,2],
		"description": "A Myling is a very vocal and active ghost. They are rumoured to be quiet when hunting their prey.",
		"strength": "Known to be quieter when hunting",
		"weakness": "More frequently make paranormal sounds"
	},
	{
		"name": "Obake",
		"evidence": [0,1,3],
		"description": "Obake are terrifying shape-shifters, capable of taking on many forms. They have been seen taking on humanoid shapes to attract their prey.",
		"strength": "When interacting with the environment, an Obake will rarely leave a trace.",
		"weakness": "May leave a 6-fingered Fingerprint Evidence when it interacts with a door or knocks in a window."
	},
	{
		"name": "Onryo",
		"evidence": [5,1,4],
		"description": "The Onryo is often referred to as \"The Wrathful Spirit.\" It steals souls from dying victims' bodies to seek revenge. This ghost has been known to fear any form of fire, and will do anything to be far from it.",
		"strength": "Extinguishing a flame can cause an Onryo to attack.",
		"weakness": "When threatened, this ghost will be less likely to attack."
	},
	{
		"name": "Raiju",
		"evidence": [0,1,6],
		"description": "A Raiju is a demon that thrives on electrical current. While generally calm, they can become agitated when overwhelmed with power.",
		"strength": "A Raiju can siphon power from nearby electrical devices, making it move faster.",
		"weakness": "Raiju are constantly disrupting electronic equipment, making it easier to track when attacking."
	},
	{
		"name": "The Twins",
		"evidence": [0,5,4],
		"description": "These ghosts have been reported to mimic each other's actions. They alternate their attacks to confuse their prey.",
		"strength": "Either Twin can be angered and initiate an attack on their prey.",
		"weakness": "The Twins will often interact with the environment at the same time."
	},
  {
    "name": "The Mimic",
    "evidence": [3,4,5],
    "description": "The Mimic is an elusive, mysterious, copycat ghost that mirrors traits and behaviours from others, including other ghost types.",
    "strength": "Can mimic the abilities and traits of other ghosts.",
    "weakness": "Will present Ghost Orbs as a secondary evidence."
  },
];

export default ghostTypes;