const evidence = [
	"EMF Level 5",    // 0
	"Ghost Orbs",     // 1
	"Ghost Writing",  // 2
	"Fingerprints",   // 3
	"Freezing Temps", // 4
	"Spirit Box",     // 5
	"D.O.T.S"         // 6
];

export default evidence;