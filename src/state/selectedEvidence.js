import { atom } from 'recoil';

const selectedEvidenceState = atom({
	key: 'selectedEvidenceState',
	default: []
});

export default selectedEvidenceState;